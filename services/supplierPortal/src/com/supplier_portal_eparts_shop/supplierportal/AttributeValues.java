/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.supplier_portal_eparts_shop.supplierportal;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AttributeValues generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`AttributeValues`")
public class AttributeValues implements Serializable {

    private Integer id;
    private Integer attributeId;
    private String attributeValue;
    private Integer partId;
    private Attributes attributes;
    private Parts parts;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`AttributeID`", nullable = true, scale = 0, precision = 10)
    public Integer getAttributeId() {
        return this.attributeId;
    }

    public void setAttributeId(Integer attributeId) {
        this.attributeId = attributeId;
    }

    @Column(name = "`AttributeValue`", nullable = true, length = 1000)
    public String getAttributeValue() {
        return this.attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    @Column(name = "`PartID`", nullable = true, scale = 0, precision = 10)
    public Integer getPartId() {
        return this.partId;
    }

    public void setPartId(Integer partId) {
        this.partId = partId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`AttributeID`", referencedColumnName = "`AttributeID`", insertable = false, updatable = false)
    public Attributes getAttributes() {
        return this.attributes;
    }

    public void setAttributes(Attributes attributes) {
        if(attributes != null) {
            this.attributeId = attributes.getAttributeId();
        }

        this.attributes = attributes;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PartID`", referencedColumnName = "`PartID`", insertable = false, updatable = false)
    public Parts getParts() {
        return this.parts;
    }

    public void setParts(Parts parts) {
        if(parts != null) {
            this.partId = parts.getPartId();
        }

        this.parts = parts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AttributeValues)) return false;
        final AttributeValues attributeValues = (AttributeValues) o;
        return Objects.equals(getId(), attributeValues.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

