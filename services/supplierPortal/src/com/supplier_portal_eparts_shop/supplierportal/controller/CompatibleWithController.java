/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.supplier_portal_eparts_shop.supplierportal.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.supplier_portal_eparts_shop.supplierportal.CompatibleWith;
import com.supplier_portal_eparts_shop.supplierportal.PartsPerCompatibleWith;
import com.supplier_portal_eparts_shop.supplierportal.service.CompatibleWithService;


/**
 * Controller object for domain model class CompatibleWith.
 * @see CompatibleWith
 */
@RestController("supplierPortal.CompatibleWithController")
@Api(value = "CompatibleWithController", description = "Exposes APIs to work with CompatibleWith resource.")
@RequestMapping("/supplierPortal/CompatibleWith")
public class CompatibleWithController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompatibleWithController.class);

    @Autowired
	@Qualifier("supplierPortal.CompatibleWithService")
	private CompatibleWithService compatibleWithService;

	@ApiOperation(value = "Creates a new CompatibleWith instance.")
	@RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public CompatibleWith createCompatibleWith(@RequestBody CompatibleWith compatibleWith) {
		LOGGER.debug("Create CompatibleWith with information: {}" , compatibleWith);

		compatibleWith = compatibleWithService.create(compatibleWith);
		LOGGER.debug("Created CompatibleWith with information: {}" , compatibleWith);

	    return compatibleWith;
	}


    @ApiOperation(value = "Returns the CompatibleWith instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public CompatibleWith getCompatibleWith(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Getting CompatibleWith with id: {}" , id);

        CompatibleWith foundCompatibleWith = compatibleWithService.getById(id);
        LOGGER.debug("CompatibleWith details with id: {}" , foundCompatibleWith);

        return foundCompatibleWith;
    }

    @ApiOperation(value = "Updates the CompatibleWith instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public CompatibleWith editCompatibleWith(@PathVariable("id") Integer id, @RequestBody CompatibleWith compatibleWith) throws EntityNotFoundException {
        LOGGER.debug("Editing CompatibleWith with id: {}" , compatibleWith.getCompatibleWithId());

        compatibleWith.setCompatibleWithId(id);
        compatibleWith = compatibleWithService.update(compatibleWith);
        LOGGER.debug("CompatibleWith details with id: {}" , compatibleWith);

        return compatibleWith;
    }

    @ApiOperation(value = "Deletes the CompatibleWith instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteCompatibleWith(@PathVariable("id") Integer id) throws EntityNotFoundException {
        LOGGER.debug("Deleting CompatibleWith with id: {}" , id);

        CompatibleWith deletedCompatibleWith = compatibleWithService.delete(id);

        return deletedCompatibleWith != null;
    }

    /**
     * @deprecated Use {@link #findCompatibleWiths(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of CompatibleWith instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompatibleWith> searchCompatibleWithsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CompatibleWiths list");
        return compatibleWithService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of CompatibleWith instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompatibleWith> findCompatibleWiths(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering CompatibleWiths list");
        return compatibleWithService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of CompatibleWith instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<CompatibleWith> filterCompatibleWiths(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering CompatibleWiths list");
        return compatibleWithService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportCompatibleWiths(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return compatibleWithService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of CompatibleWith instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countCompatibleWiths( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting CompatibleWiths");
		return compatibleWithService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getCompatibleWithAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return compatibleWithService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/partsPerCompatibleWiths", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the partsPerCompatibleWiths instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<PartsPerCompatibleWith> findAssociatedPartsPerCompatibleWiths(@PathVariable("id") Integer id, Pageable pageable) {

        LOGGER.debug("Fetching all associated partsPerCompatibleWiths");
        return compatibleWithService.findAssociatedPartsPerCompatibleWiths(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service CompatibleWithService instance
	 */
	protected void setCompatibleWithService(CompatibleWithService service) {
		this.compatibleWithService = service;
	}

}

