/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.supplier_portal_eparts_shop.supplierportal.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.supplier_portal_eparts_shop.supplierportal.AttributesPerCategory;
import com.supplier_portal_eparts_shop.supplierportal.Categories;
import com.supplier_portal_eparts_shop.supplierportal.Parts;


/**
 * ServiceImpl object for domain model class Categories.
 *
 * @see Categories
 */
@Service("supplierPortal.CategoriesService")
public class CategoriesServiceImpl implements CategoriesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoriesServiceImpl.class);

    @Autowired
	@Qualifier("supplierPortal.AttributesPerCategoryService")
	private AttributesPerCategoryService attributesPerCategoryService;

    @Autowired
	@Qualifier("supplierPortal.PartsService")
	private PartsService partsService;

    @Autowired
    @Qualifier("supplierPortal.CategoriesDao")
    private WMGenericDao<Categories, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Categories, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "supplierPortalTransactionManager")
    @Override
	public Categories create(Categories categories) {
        LOGGER.debug("Creating a new Categories with information: {}", categories);
        Categories categoriesCreated = this.wmGenericDao.create(categories);
        if(categoriesCreated.getAttributesPerCategories() != null) {
            for(AttributesPerCategory attributesPerCategorie : categoriesCreated.getAttributesPerCategories()) {
                attributesPerCategorie.setCategories(categoriesCreated);
                LOGGER.debug("Creating a new child AttributesPerCategory with information: {}", attributesPerCategorie);
                attributesPerCategoryService.create(attributesPerCategorie);
            }
        }

        if(categoriesCreated.getCategoriesesForParentId() != null) {
            for(Categories categoriesesForParentId : categoriesCreated.getCategoriesesForParentId()) {
                categoriesesForParentId.setCategoriesByParentId(categoriesCreated);
                LOGGER.debug("Creating a new child Categories with information: {}", categoriesesForParentId);
                create(categoriesesForParentId);
            }
        }

        if(categoriesCreated.getPartses() != null) {
            for(Parts partse : categoriesCreated.getPartses()) {
                partse.setCategories(categoriesCreated);
                LOGGER.debug("Creating a new child Parts with information: {}", partse);
                partsService.create(partse);
            }
        }
        return categoriesCreated;
    }

	@Transactional(readOnly = true, value = "supplierPortalTransactionManager")
	@Override
	public Categories getById(Integer categoriesId) throws EntityNotFoundException {
        LOGGER.debug("Finding Categories by id: {}", categoriesId);
        Categories categories = this.wmGenericDao.findById(categoriesId);
        if (categories == null){
            LOGGER.debug("No Categories found with id: {}", categoriesId);
            throw new EntityNotFoundException(String.valueOf(categoriesId));
        }
        return categories;
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
	@Override
	public Categories findById(Integer categoriesId) {
        LOGGER.debug("Finding Categories by id: {}", categoriesId);
        return this.wmGenericDao.findById(categoriesId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "supplierPortalTransactionManager")
	@Override
	public Categories update(Categories categories) throws EntityNotFoundException {
        LOGGER.debug("Updating Categories with information: {}", categories);
        this.wmGenericDao.update(categories);

        Integer categoriesId = categories.getCategoryId();

        return this.wmGenericDao.findById(categoriesId);
    }

    @Transactional(value = "supplierPortalTransactionManager")
	@Override
	public Categories delete(Integer categoriesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Categories with id: {}", categoriesId);
        Categories deleted = this.wmGenericDao.findById(categoriesId);
        if (deleted == null) {
            LOGGER.debug("No Categories found with id: {}", categoriesId);
            throw new EntityNotFoundException(String.valueOf(categoriesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "supplierPortalTransactionManager")
	@Override
	public Page<Categories> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Categories");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
    @Override
    public Page<Categories> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Categories");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service supplierPortal for table Categories to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "supplierPortalTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
    @Override
    public Page<AttributesPerCategory> findAssociatedAttributesPerCategories(Integer categoryId, Pageable pageable) {
        LOGGER.debug("Fetching all associated attributesPerCategories");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("categories.categoryId = '" + categoryId + "'");

        return attributesPerCategoryService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
    @Override
    public Page<Categories> findAssociatedCategoriesesForParentId(Integer categoryId, Pageable pageable) {
        LOGGER.debug("Fetching all associated categoriesesForParentId");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("categoriesByParentId.categoryId = '" + categoryId + "'");

        return findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "supplierPortalTransactionManager")
    @Override
    public Page<Parts> findAssociatedPartses(Integer categoryId, Pageable pageable) {
        LOGGER.debug("Fetching all associated partses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("categories.categoryId = '" + categoryId + "'");

        return partsService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service AttributesPerCategoryService instance
	 */
	protected void setAttributesPerCategoryService(AttributesPerCategoryService service) {
        this.attributesPerCategoryService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service PartsService instance
	 */
	protected void setPartsService(PartsService service) {
        this.partsService = service;
    }

}

