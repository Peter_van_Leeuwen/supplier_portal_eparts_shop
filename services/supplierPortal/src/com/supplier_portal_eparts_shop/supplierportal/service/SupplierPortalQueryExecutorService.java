/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/

package com.supplier_portal_eparts_shop.supplierportal.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.supplier_portal_eparts_shop.supplierportal.models.query.*;

public interface SupplierPortalQueryExecutorService {

    Page<StatusFilterQueryResponse> executeStatusFilterQuery(Pageable pageable);

    Downloadable exportStatusFilterQuery(ExportType exportType, Pageable pageable);

    Page<StatusCountQueryResponse> executeStatusCountQuery(Pageable pageable);

    Downloadable exportStatusCountQuery(ExportType exportType, Pageable pageable);

    Page<NextSkuResponse> executeNext_sku(Pageable pageable);

    Downloadable exportNext_sku(ExportType exportType, Pageable pageable);

    Page<NumPartsWithStatusForVendorResponse> executeNum_parts_with_status_for_vendor(String status, Integer vendorId, Pageable pageable);

    Downloadable exportNum_parts_with_status_for_vendor(ExportType exportType, String status, Integer vendorId, Pageable pageable);

}


