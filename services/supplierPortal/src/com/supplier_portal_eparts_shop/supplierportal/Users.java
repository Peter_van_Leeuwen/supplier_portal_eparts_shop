/*Copyright (c) 2017-2018 vanenburgsoftware.com All Rights Reserved.
 This software is the confidential and proprietary information of vanenburgsoftware.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with vanenburgsoftware.com*/
package com.supplier_portal_eparts_shop.supplierportal;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * Users generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`Users`")
public class Users implements Serializable {

    private Integer id;
    private String username;
    private String password;
    private String role;
    private Integer vendorId;
    private Vendors vendors;
    private List<Parts> partsesForCreatedBy;
    private List<Parts> partsesForModifiedBy;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`ID`", nullable = false, scale = 0, precision = 10)
    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "`Username`", nullable = true, length = 255)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "`Password`", nullable = true, length = 255)
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "`Role`", nullable = true, length = 255)
    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Column(name = "`VendorID`", nullable = true, scale = 0, precision = 10)
    public Integer getVendorId() {
        return this.vendorId;
    }

    public void setVendorId(Integer vendorId) {
        this.vendorId = vendorId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`VendorID`", referencedColumnName = "`VendorID`", insertable = false, updatable = false)
    public Vendors getVendors() {
        return this.vendors;
    }

    public void setVendors(Vendors vendors) {
        if(vendors != null) {
            this.vendorId = vendors.getVendorId();
        }

        this.vendors = vendors;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "usersByCreatedBy")
    public List<Parts> getPartsesForCreatedBy() {
        return this.partsesForCreatedBy;
    }

    public void setPartsesForCreatedBy(List<Parts> partsesForCreatedBy) {
        this.partsesForCreatedBy = partsesForCreatedBy;
    }

    @JsonInclude(Include.NON_EMPTY)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "usersByModifiedBy")
    public List<Parts> getPartsesForModifiedBy() {
        return this.partsesForModifiedBy;
    }

    public void setPartsesForModifiedBy(List<Parts> partsesForModifiedBy) {
        this.partsesForModifiedBy = partsesForModifiedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Users)) return false;
        final Users users = (Users) o;
        return Objects.equals(getId(), users.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}

