/**This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
package com.supplier_portal_eparts_shop.md5encryption.controller;

import com.supplier_portal_eparts_shop.md5encryption.MD5Encryption;
import java.lang.String;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

@RestController
@RequestMapping(value = "/mD5Encryption")
public class MD5EncryptionController {

    @Autowired
    private MD5Encryption mD5Encryption;

    @RequestMapping(value = "/md5Spring", produces = "application/json", method = RequestMethod.GET)
    public String md5Spring(@RequestParam(value = "text", required = false) String text) {
        return mD5Encryption.md5Spring(text);
    }
}
