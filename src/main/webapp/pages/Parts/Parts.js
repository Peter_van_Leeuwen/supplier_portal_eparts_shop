Application.$controller("PartsPageController", ["$scope", "DialogService",
    function($scope, DialogService) {
        "use strict";

        /* perform any action on widgets within this block */
        $scope.onPageReady = function() {
            /*
             * widgets can be accessed through '$scope.Widgets' property here
             * e.g. to get value of text widget named 'username' use following script
             * '$scope.Widgets.username.datavalue'
             */
        };

        $scope.requested_parts_filterClick = function($event, $isolateScope) {
            $scope.Variables.SupplierPortalPartsData.setFilter("Status", "Requested");
            $scope.Variables.SupplierPortalPartsData.listRecords();
            $scope.Widgets.parts_grid.nodatamessage = "No requested parts found.";
        };



        $scope.button_refreshClick = function($event, $isolateScope) {
            $scope.Variables.SupplierPortalPartsData.setFilter("Status", "");
            $scope.Widgets.parts_grid.refreshData();
            $scope.Widgets.parts_grid.nodatamessage = "No data found.";
        };


        $scope.create_part_btnClick = function($event, $isolateScope) {
            /*if ($scope.Variables.curSupplier.dataSet.allotment > $scope.Variables.numOfProductsForSupplier.dataSet.value) {*/
            $scope.Variables.goToPage_Create_Part.invoke();
            /*} else {
            DialogService.showDialog('maxAllotmentInfoDialog');
            }*/
        };

    }
]);

Application.$controller("livefilter1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("parts_gridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customRowAction = function($event, $rowData) {
            var lv = $scope.Variables.SupplierPortalPartsDataUpdate;
            var rowUpdate = $rowData;
            rowUpdate.status = "Approved";
            lv.updateRecord({
                row: rowUpdate
            }, function(data) {
                $scope.refreshData();
                $scope.Widgets.grid1.redraw(true);
            }, function(error) {

            });
        };


        $scope.customRow1Action = function($event, $rowData) {
            var lv = $scope.Variables.SupplierPortalPartsDataUpdate;
            var rowUpdate = $rowData;
            rowUpdate.status = "Rejected";
            lv.updateRecord({
                row: rowUpdate
            }, function(data) {
                $scope.refreshData();
                $scope.Widgets.grid1.redraw(true);
            }, function(error) {

            });
        };

    }
]);

Application.$controller("maxAllotmentInfoDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("categories_gridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("brands_gridController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("liveform2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);