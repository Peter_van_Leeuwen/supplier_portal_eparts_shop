Application.$controller("Create_Part_SimplePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.save_simple_part_btnClick = function($event, $isolateScope) {
        $scope.Variables.loggedInUser.getData().id;
    };

    $scope.liveform_create_partResult = function($event, $operation, $data) {
        // if ($scope.Widgets.category.datavalue !== null) {

        //     var lv = $scope.Variables.insertCategoryPerPart;
        //     lv.createRecord({
        //         row: {
        //             'categoryId': $scope.Widgets.category.datavalue,
        //             'partId': $data.partId
        //         }
        //     }, function(data) {}, function(error) {});
        // }
    };


    $scope.liveform_create_partBeforeservicecall = function($event, $operation, $data) {
        $data.partCode = "E-" + $data.partNumberVendor;
    };

}]);

Application.$controller("liveform_create_partController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.customAction = function($event) { //action for "Save as requested" button
            var fields = $scope.formFields;
            console.log(fields);
            for (var i = 0; i < fields.length; i++) {
                console.log(fields[i].name);
                if (!("value" in fields[i]) || fields[i].value === "") {
                    if (fields[i].name === "partId" || fields[i].name === "partCode") { //these fields are added later into the database
                        continue;
                    }
                    $scope.Variables.notReadyForRequested.invoke();
                    return;
                }
                if (fields[i].name === "status") {
                    $scope.formFields[i].value = "Requested"
                }
            }
            $scope.save()
        };


        $scope.select_disciplineChange = function($event, $isolateScope, newVal, oldVal) {
            $scope.formFields.forEach(function(field) {
                if (field.name === "skunumber") {
                    field.value = newVal + field.value.substring(2);
                    return
                }
            });
        };

    }
]);

Application.$controller("add_brand_dialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("liveform1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);