Application.$controller("Parts_CataloguePageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets within this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        console.log($scope.Variables.PartsCount.dataSet.value);
        $scope.Variables.PartsCount.dataSet.value = 99;
    };




    $scope.statusFilterQuerySvcVaronSuccess = function(variable, data) {
        debugger
        var valArr = [];
        var percentageArr = [];


        if (data.content) {
            //form the relative percentage array from the query response
            var respLen = data.content.length;
            for (var i = 0; i < respLen; i++) {
                if (data.content[i]) {
                    valArr.push(data.content[i].count_status_);
                }
            }
            var maxVal = Math.max.apply(Math, valArr);
            percentageArr = valArr.map(function(item) {
                if (maxVal > 0) {
                    return ((item / maxVal) * 100)
                } else {
                    return 0;
                }
            });

            //form the consolidatedStatusArr to make the data binding with the Progress Bar widget
            $scope.consolidatedStatusArr = {
                statusValue: []
            }

            for (var i = 0; i < respLen; i++) {
                if (data.content[i]) {
                    var statusDetails = {
                        name: "",
                        unit: "",
                        percentage: ""
                    };

                    statusDetails.name = data.content[i].status;
                    statusDetails.unit = data.content[i].count_status_;
                    statusDetails.percentage = percentageArr[i];
                    $scope.consolidatedStatusArr.statusValue.push(statusDetails);
                }
            }
        }

    };

}]);