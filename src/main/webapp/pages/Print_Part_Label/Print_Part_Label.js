Application.$controller("Print_Part_LabelPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        console.log($scope.Widgets.iframe_part_label);
        $scope.Widgets.iframe_part_label.setProperty("source", "services/generateReports/generatePdfReport?jrxml=supplier_label2.jrxml&database=Parts&partID=18");
    };

}]);