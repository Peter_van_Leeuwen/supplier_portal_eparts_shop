var _WM_APP_PROPERTIES = {
  "activeTheme" : "eparts.shop",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "Supplier Portal eparts.shop",
  "homePage" : "Main",
  "name" : "Supplier_Portal_eparts_shop",
  "platformType" : "WEB",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};